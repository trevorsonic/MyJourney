//
//  JourneyCellVC.swift
//  MyJourney
//
//  Created by dejaWorks on 19/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

class JourneyCellVC: NSObject {

    // MARK: - Holder View connection
    // Holder is a uiview in design mode to place the real Journey info view
    // in this way; design can be made on storyboard and on the fly we can add the
    // UI programmaticaly. Advantage is using the same desing we made for UITableViewCell
    fileprivate unowned let journeyInfoHolder: UIView
    fileprivate var journeyInfo:JourneyCell!
    
    // MARK: - Set view with init
    init(_ journeyInfoHolder: UIView) {
        self.journeyInfoHolder = journeyInfoHolder
    }
    
    // Inject the information into cell with given journey object
    var theJourney:Journey!{
        didSet{
            journeyInfo.journeyName.text    = theJourney.name
            journeyInfo.startDate.text      = theJourney.startDate
            journeyInfo.endDate.text        = theJourney.endDate
            journeyInfo.startTime.text      = theJourney.startTime
            journeyInfo.endTime.text        = theJourney.endTime
            journeyInfo.duration.text       = theJourney.duration
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func setup(){
        addJourneyInfoView()
    }

    func addJourneyInfoView(){
        
        // Journey info view is the same as UITableViewCell
        journeyInfo = Bundle.main.loadNibNamed(JourneyCell.identifier, owner: self, options: nil)?.first as? JourneyCell
        
        // add info view on provided holderView
        journeyInfoHolder.addSubview(journeyInfo!)
        
        journeyInfo.frame.origin.y = 30
        
        // Set constraints for info view
        // TODO: Constraints below don't work / change anything (yet)
        journeyInfo?.contentView.widthAnchor.constraint(equalTo: journeyInfoHolder.widthAnchor)
        journeyInfo?.contentView.heightAnchor.constraint(equalTo : journeyInfoHolder.heightAnchor)
        
        journeyInfo?.centerXAnchor.constraint(equalTo: journeyInfoHolder.centerXAnchor)
        journeyInfo?.centerYAnchor.constraint(equalTo: journeyInfoHolder.centerYAnchor)
        
        journeyInfo?.topAnchor.constraint(equalTo: journeyInfoHolder.topAnchor, constant: 0).isActive = true
        journeyInfo?.bottomAnchor.constraint(equalTo: journeyInfoHolder.bottomAnchor, constant: 0).isActive = true
        journeyInfo?.leadingAnchor.constraint(equalTo: journeyInfoHolder.leadingAnchor, constant: 0).isActive = true
        journeyInfo?.trailingAnchor.constraint(equalTo: journeyInfoHolder.trailingAnchor, constant: 0).isActive = true
        
        
        journeyInfo?.contentView.topAnchor.constraint(equalTo: journeyInfoHolder.topAnchor)
        journeyInfo?.contentView.bottomAnchor.constraint(equalTo: journeyInfoHolder.bottomAnchor)
        journeyInfo?.contentView.leadingAnchor.constraint(equalTo: journeyInfoHolder.leadingAnchor)
        journeyInfo?.contentView.trailingAnchor.constraint(equalTo: journeyInfoHolder.trailingAnchor)
        
    }
}
