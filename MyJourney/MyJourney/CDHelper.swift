//
//  CDHelper.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import CoreData

// MARK: - CoreData Helper Singleton Class
// Here there are many commented code. They are some preparation for iCloud data sync.


private let _shared = CDHelper()
class CDHelper : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : CDHelper {
        return _shared
    }
    // MARK: - PATHS
    lazy var storesDirectory: URL? = {
        let fm = FileManager.default
        let urls = fm.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    lazy var localStoreURL: URL? = {
        if let url = self.storesDirectory?.appendingPathComponent("MyJourneyStore.sqlite") {
            print("localStoreURL = \(url)")
            return url
        }
        return nil
    }()
    lazy var modelURL: URL = {
        let bundle = Bundle.main
        if let url = bundle.url(forResource: "Data", withExtension: "momd") {
            return url
        }
        print("CRITICAL - Managed Object Model file not found")
        abort()
    }()
    
    // MARK: - CONTEXT
    lazy var context: NSManagedObjectContext = {
        let moc = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = self.coordinator
        // iCloud merge policy
        //moc.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.mergeByPropertyObjectTrumpMergePolicyType);
        
        return moc
    }()
    
    // MARK: - MODEL
    lazy var model: NSManagedObjectModel = {
        return NSManagedObjectModel(contentsOf:self.modelURL)!
    }()
    
    // MARK: - COORDINATOR
    lazy var coordinator: NSPersistentStoreCoordinator = {
        return NSPersistentStoreCoordinator(managedObjectModel:self.model)
    }()
    
    // MARK: - STORE
    lazy var localStore: NSPersistentStore? = {
        // Original -> let options:[AnyHashable: Any] = [NSSQLitePragmasOption:["journal_mode":"DELETE"]]
        // Create options (from my stack)
        // NSInferMappingModelAutomaticallyOption: true is for lightweight migration.
        let options = [NSMigratePersistentStoresAutomaticallyOption: true,
                       NSInferMappingModelAutomaticallyOption: true]
        
        // iCloud add in option
        //NSPersistentStoreUbiquitousContentNameKey: "MyJourneyStore"] as [String : Any]
        
        // iCloud add in Info.plist
        /*
         <key>NSUbiquitousContainers</key>
         <dict>
         <key>iCloud.com.dejaworks.MyJourney</key>
         <dict>
         <key>NSUbiquitousContainerIsDocumentScopePublic</key>
         <true/>
         <key>NSUbiquitousContainerName</key>
         <string>MyJourney</string>
         <key>NSUbiquitousContainerSupportedFolderLevels</key>
         <string>Any</string>
         </dict>
         </dict>
         */
        
        
        var _localStore:NSPersistentStore?
        do {
            _localStore = try self.coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: self.localStoreURL, options: options)
            
            
            return _localStore
        } catch {
            return nil
        }
    }()
    
    // MARK: - SETUP
    required override init() {
        super.init()
        self.setupCoreData()
        //self.registerCoordinatorForStoreNotifications()
    }
    func setupCoreData() {
        _ = self.localStore
    }
    
    // MARK: - SAVING
    class func save(_ moc:NSManagedObjectContext) {
        
        moc.performAndWait {
            
            if moc.hasChanges {
                
                do {
                    try moc.save()
                    print("SAVED context \(moc.description)")
                } catch {
                    print("ERROR saving context \(moc.description) - \(error)")
                }
            } else {
                print("SKIPPED saving context \(moc.description) because there are no changes")
            }
            if let parentContext = moc.parent {
                save(parentContext)
            }
        }
    }
    class func saveSharedContext() {
        save(shared.context)
    }
    // MARK: - ACTIONS
    // MARK: Merge
    func mergeChanges(_ notification: NSNotification) {
        NSLog("mergeChanges notif:\(notification)")
        let moc = context
        moc.perform {
            moc.mergeChanges(fromContextDidSave: notification as Notification)
            self.postRefetchDatabaseNotification()
        }
        
    }
    // MARK: Process
    func postRefetchDatabaseNotification() {
        //        DispatchQueue.main.asynchronously(execute: {
        //            () -> Void in
        //
        //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "storeDidChange"), object: nil)
        //
        //        })
        //
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "storeDidChange"), object: nil)
        }
        
    }
    // MARK: - iCloud
    // This handles the updates to the data via iCLoud updates
    func handleStoresWillChange(notification: NSNotification){
        print("+handleStoresWillChange here. Empty function.")
        NSLog("storesWillChange notif:\(notification)");
        
        let moc = context
        moc.performAndWait {
            
            if moc.hasChanges {
                
                CDHelper.saveSharedContext()
                //                    NSLog("Save error: \(error)");
            } else {
                // drop any managed objects
            }
            moc.reset();
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "storeWillChange"), object: nil)
        
        
    }
    func handleStoresDidChange(notification: NSNotification){
        print("+handleStoresDidChange here. Empty function.")
        NSLog("storesDidChange posting notif");
        self.postRefetchDatabaseNotification();
        //Sends notification to view controllers to reload data
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "storeDidChange"), object: nil)
    }
    func handleStoresWillRemove(){
        print("+handleStoresWillRemove here. Empty function.")
    }
    func handleStoreChangedUbiquitousContent(notification: NSNotification){
        print("+handleStoreChangedUbiquitousContent here. Empty function.")
        self.mergeChanges(notification);
    }
    func registerCoordinatorForStoreNotifications () {
        
        print("\nThis handles the updates to the data via iCLoud updates")
        
        let nc : NotificationCenter = NotificationCenter.default
        
        nc.addObserver(self,
                       selector: #selector(CDHelper.shared.handleStoresWillChange),
                       name: NSNotification.Name.NSPersistentStoreCoordinatorStoresWillChange,
                       object: self.coordinator)
        
        nc.addObserver(self,
                       selector: #selector(CDHelper.shared.handleStoresDidChange),
                       name: NSNotification.Name.NSPersistentStoreCoordinatorStoresDidChange,
                       object: self.coordinator)
        
        nc.addObserver(self,
                       selector: #selector(CDHelper.shared.handleStoresWillRemove),
                       name: NSNotification.Name.NSPersistentStoreCoordinatorWillRemoveStore,
                       object: self.coordinator)
        
        nc.addObserver(self,
                       selector: #selector(CDHelper.shared.handleStoreChangedUbiquitousContent),
                       name: NSNotification.Name.NSPersistentStoreDidImportUbiquitousContentChanges,
                       object: self.coordinator)
    }
}

