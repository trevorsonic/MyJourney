//
//  NotificationExtension.swift
//  MyJourney
//
//  Created by dejaWorks on 19/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation

extension Notification.Name {
    // When tracker updated location
    static let didUpdateLocationsEvent = Notification.Name("DWdidUpdateLocationsEvent")
}
