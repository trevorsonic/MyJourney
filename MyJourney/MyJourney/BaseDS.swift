//
//  BaseDS.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import CoreData
// MARK: - Base Data Service
class BaseDS{

    var context = CDHelper.shared.context

    // MARK: Set defaults for new instance
    func setDefaults(_ base: Base){
        base.name       =   ""
        base.createdAt  =   NSDate()
        base.updatedAt  =   NSDate()
    }

    // MARK: String to Date convertor
    public func dateFromString(_ strDate: String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        let date = dateFormatter.date(from: strDate)
        return date!
    }
    
    // MARK: Delete
    internal func delete(_ item: NSManagedObject){
        context.delete(item)
        CDHelper.saveSharedContext()
    }
    
    // MARK: Save
    func saveContext(){
        CDHelper.saveSharedContext()
    }
}

