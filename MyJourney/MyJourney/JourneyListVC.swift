//
//  JourneyListVC.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

//  MARK: - Self Delegate definitions
protocol JourneyDetailVCDelegate {
    func willClose()
    func selectedJourney(_ journey:Journey)
}

class JourneyListVC: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource{

    var delegate:JourneyDetailVCDelegate?
    // MARK: - UI Outlet Connections
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButtonOutlet: UIButton!
    
    // MARK: - UI Actions
    @IBAction func closeButton(_ sender: Any) {
        closeSideMenu()
    }
    
    // MARK: - DELEGATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // Bind Journey Fetched Result Controller
    fileprivate lazy var journeyFRC: JourneyFRC = {
        let delegate = JourneyFRC(tableView: self.tableView)
        delegate.onUpdate = {
            (cell: UITableViewCell, object: AnyObject) in
            self.configureCell(cell, journey: object as! Journey)
            
        }
        return delegate
    }()
    
    // MARK: - JourneyCell Render
    fileprivate func configureCell(_ cell: UITableViewCell, journey: Journey) {
        if let journeyCell = cell as? JourneyCell {
            // injection the data into the  cell view
            journeyCell.journey = journey
        }
    }
    // MARK: - DELEGATION <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    
    // MARK: -  private vars
    private var _isSideMenuPresent = false
    
    
    // MARK: -  public vars
    var isSideMenuPresent:Bool{
        get{
            return _isSideMenuPresent
        }
    }
    
    // MARK: - INIT
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.register(JourneyCell.nib, forCellReuseIdentifier: JourneyCell.identifier)
        
        
        tableView.delegate   = self
        tableView.dataSource = self
        
        // Check if this is first run. If so add test journey
        if JourneyDS.shared.totalQty < 1 {
            JourneyDS.shared.createDefaultJourney()
        }
        loadData()
    }
    // MARK: - Table view load data
    public func loadData(){
        journeyFRC.loadData()
    }
    // MARK: - UITableView implementations | 3 Delegate Implementation ++++
    // MARK: - 1: Section count
    func numberOfSections(in tableView: UITableView) -> Int {
        return journeyFRC.resultsController.sections!.count;
    }
    // MARK: - 2: Row count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return journeyFRC.resultsController.sections![section].numberOfObjects
    }
    // MARK: - 3: Cell render
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let journeyCell = tableView.dequeueReusableCell(withIdentifier: "JourneyCell", for: indexPath) as! JourneyCell
        let journey = journeyFRC.resultsController.object(at: indexPath) as! Journey
        configureCell(journeyCell, journey: journey)
        return journeyCell
    }
    // MARK: - Table View Edit Delete
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        // Delete
        if editingStyle == .delete {
            print("UITableViewCellEditingStyle: .delete")
            let journey = journeyFRC.resultsController.object(at: indexPath) as! Journey
            deleteJourney(journey)
        }
    }
    func deleteJourney(_ journey: Journey) {
        JourneyDS.shared.delete(journey)
    }
    // MARK: - When item selected in table view; inform delegate's selected cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let journey = journeyFRC.resultsController.object(at: indexPath) as! Journey
        delegate?.selectedJourney(journey)
        closeSideMenu()
    }
    // MARK: - Open / Close Animations as Side menu
    func closeSideMenu(){
        self.delegate?.willClose()
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
            self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
        }, completion: { (finished) -> Void in
            print("side menu close ")
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            self._isSideMenuPresent = false
            
        })
    }
    // Touching anywhere on view helps to close
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //closeSideMenu()
    }
    func openSideMenu(){
        
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: { () -> Void in
            
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            
        }, completion: { (finished) -> Void in
            print("side menu open ")
            self._isSideMenuPresent = true
        })
    }
}
