//
//  CoordDS.swift
//  MyJourney
//
//  Created by dejaWorks on 14/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Coord Data Service  (Coord : Coordinates object)

private let _shared = CoordDS()
class CoordDS: BaseDS {
    
    // MARK: - SHARED INSTANCE
    class var shared : CoordDS {
        return _shared
    }
    
    // MARK: - Instantiate Coord object
    var newEmptyCoord:Coord{
        get{
            let emptyCoord = NSEntityDescription.insertNewObject(forEntityName: "Coord", into: context) as! Coord
            setDefaults(emptyCoord)
           
            return emptyCoord
        }
    }
    
}
