//
//  BaseFRCD.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreData

// MARK: -  Journey Fetched Results Controller Delegate
class BaseFRCD: NSObject, NSFetchedResultsControllerDelegate {
    
    fileprivate unowned let tableView: UITableView
    
    var resultsController:  NSFetchedResultsController<NSManagedObject>!
    
    open var onUpdate: ((_ cell: UITableViewCell, _ object: AnyObject) -> Void)?
    open var ignoreNextUpdates: Bool = false
    
    
    // MARK: - INIT & BIND TableView
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    // MARK: - LOAD Data
    public func loadData()
    {
        // Implement this like below for every FRC
        
        /*
        // Example Fetched Result Controller request
         
        let request:NSFetchRequest<Journey> = Journey.fetchRequest()
        let sortByCreatedAt = NSSortDescriptor(key: "createdAt", ascending: false)
        
        request.sortDescriptors = [sortByCreatedAt]
        
        resultsController = NSFetchedResultsController(fetchRequest: request as! NSFetchRequest<NSManagedObject>,
                                                       managedObjectContext: CDHelper.shared.context,
                                                       sectionNameKeyPath: nil,
                                                       cacheName: nil)
        
        
        do{
            try resultsController.performFetch()
        }catch let e as NSError {
            print("Error BaseFRCD -> resultsController.performFetch() -> \(e.localizedDescription)")
        }
         
        */
        
        //resultsController.delegate = self
    }

    // MARK: - WillChange
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)  {
        if ignoreNextUpdates {
            return
        }
        tableView.beginUpdates()
    }
    
    // MARK: - SECTION changes
    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType)  {
        if ignoreNextUpdates {
            return
        }
        
        switch type {
        case .insert:
            //            sectionsBeingAdded.append(sectionIndex)
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            //            sectionsBeingRemoved.append(sectionIndex)
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    // MARK: - ROW changes
    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if ignoreNextUpdates {
            return
        }
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) {
                onUpdate?(cell, anObject as AnyObject)
            }
        case .move:

            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
           
        }
    }
    // MARK: - DidChange
    open func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>)  {
        if !ignoreNextUpdates {
            tableView.endUpdates()
        }
        
        ignoreNextUpdates = false
    }
}
