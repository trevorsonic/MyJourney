//
//  JourneyFRC.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Journey Fetched Results Controller
class JourneyFRC: BaseFRCD {
    
    // MARK: - Requesting data vi Fetched Results Controller
    public override func loadData()
    {
        let request:NSFetchRequest<Journey> = Journey.fetchRequest()
        let sortByCreatedAt = NSSortDescriptor(key: "createdAt", ascending: false)
        
        request.sortDescriptors = [sortByCreatedAt]
        
        resultsController = NSFetchedResultsController(fetchRequest: request as! NSFetchRequest<NSManagedObject>,
                                                       managedObjectContext: CDHelper.shared.context,
                                                       sectionNameKeyPath: nil,
                                                       cacheName: nil)
        
        
        do{
            try resultsController.performFetch()
        }catch let e as NSError {
            print("Error BaseFRCD -> resultsController.performFetch() -> \(e.localizedDescription)")
        }
        
        resultsController.delegate = self
    }
}
