//
//  JourneyCell.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit

class JourneyCell: UITableViewCell {

    //UI Outlet connections
    
    @IBOutlet weak var journeyName: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var duration: UILabel!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    // TODO: move this injection in future controller
    // Inject the information into cell with given journey object
    var journey:Journey!{
        didSet{
            journeyName.text    = journey.name
            startDate.text      = journey.startDate
            endDate.text        = journey.endDate
            startTime.text      = journey.startTime
            endTime.text        = journey.endTime
            duration.text       = journey.duration
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
