//
//  Coord+CoreDataProperties.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import CoreData
import MapKit

extension Coord {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Coord> {
        return NSFetchRequest<Coord>(entityName: "Coord")
    }
    
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var journey: Coord?
    
}


extension Coord {
    var coord2D: CLLocationCoordinate2D{
        //print("self.latitude, self.longitude :\(self.latitude) | \(self.longitude)")
        return CLLocationCoordinate2DMake( self.latitude, self.longitude)
    }
}
