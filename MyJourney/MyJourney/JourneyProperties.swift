//
//  Journey+CoreDataProperties.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import CoreData
import MapKit

extension Journey {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Journey> {
        return NSFetchRequest<Journey>(entityName: "Journey")
    }
    
    @NSManaged public var coords: NSOrderedSet?
    
}

// MARK: Generated accessors for coords
extension Journey {
    
    @objc(insertObject:inCoordsAtIndex:)
    @NSManaged public func insertIntoCoords(_ value: Coord, at idx: Int)
    
    @objc(removeObjectFromCoordsAtIndex:)
    @NSManaged public func removeFromCoords(at idx: Int)
    
    @objc(insertCoords:atIndexes:)
    @NSManaged public func insertIntoCoords(_ values: [Coord], at indexes: NSIndexSet)
    
    @objc(removeCoordsAtIndexes:)
    @NSManaged public func removeFromCoords(at indexes: NSIndexSet)
    
    @objc(replaceObjectInCoordsAtIndex:withObject:)
    @NSManaged public func replaceCoords(at idx: Int, with value: Coord)
    
    @objc(replaceCoordsAtIndexes:withCoords:)
    @NSManaged public func replaceCoords(at indexes: NSIndexSet, with values: [Coord])
    
    @objc(addCoordsObject:)
    @NSManaged public func addToCoords(_ value: Coord)
    
    @objc(removeCoordsObject:)
    @NSManaged public func removeFromCoords(_ value: Coord)
    
    @objc(addCoords:)
    @NSManaged public func addToCoords(_ values: NSOrderedSet)
    
    @objc(removeCoords:)
    @NSManaged public func removeFromCoords(_ values: NSOrderedSet)
    
}

// Formated properties as string
extension Journey {
    var startDate:String {
        get{
            if self.coords == nil {return "..."}
            
            if let firstCoord = self.coords?.array.first{
                
                if let date = (firstCoord as! Coord).createdAt {
                    return self.dateFormatter(date as Date)
                }
            }
            return "..."
        }
    }
    var endDate:String {
        get{
            if self.coords == nil {return "..."}
            
            if let lastCoord = self.coords?.array.last{
                
                if let date = (lastCoord as! Coord).createdAt {
                    return self.dateFormatter(date as Date)
                }
            }
            return "..."
        }
    }
    var startTime:String {
        get{
            if self.coords == nil {return "..."}
            
            if let firstCoord = self.coords?.array.first{
                
                if let date = (firstCoord as! Coord).createdAt {
                    return self.timeFormatter(date as Date)
                }
            }
            return "..."
        }
    }
    var endTime:String {
        get{
            if self.coords == nil {return "..."}
            
            if let lastCoord = self.coords?.array.last{
                
                if let date = (lastCoord as! Coord).createdAt {
                    return self.timeFormatter(date as Date)
                }
            }
            return "..."
        }
    }
    var duration:String{
        get{
            if self.coords == nil {return "..."}
            
            if let firstCoord = self.coords?.array.first,
                let lastCoord = self.coords?.array.last {
                
                let dateComponentsFormatter = DateComponentsFormatter()
                dateComponentsFormatter.allowedUnits = [.day,.hour,.minute,.second]
                dateComponentsFormatter.maximumUnitCount = 3
                dateComponentsFormatter.unitsStyle = .abbreviated
                
                let resutStr = dateComponentsFormatter.string(from: (firstCoord as! Coord).createdAt! as Date, to: (lastCoord as! Coord).createdAt! as Date)
                
                return resutStr ?? "..."
            }
            
            return "..."
        }
    }
    private func dateFormatter(_ date:Date)->String{
        
        let dateStr = DateFormatter.localizedString(from: date, dateStyle: DateFormatter.Style.short, timeStyle: DateFormatter.Style.none)
        return dateStr
        
    }
    private func timeFormatter(_ date:Date)->String{
        
        let timeStr = DateFormatter.localizedString(from: date, dateStyle: DateFormatter.Style.none, timeStyle: DateFormatter.Style.short)
        return timeStr
        
    }
}
