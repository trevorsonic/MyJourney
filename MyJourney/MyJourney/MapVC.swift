//
//  ViewController.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapVC: NSObject, MKMapViewDelegate{
    

    // MARK: - View + Model Connections
    // Map View Binding
    fileprivate unowned let mapView: MKMapView
    var trackerModel:TrackerModel!
    
    // MARK: - INIT & BIND with View
    init(_ mapView: MKMapView) {
        self.mapView = mapView
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //  MARK: - private vars
    // lastPolygonIndex helps to remember already drown polyline index
    private var lastPolygonIndex = 0
    private var beginAndEndCoords:[Coord]!
    private var firstAnnotation: MKPointAnnotation!
    private var lastAnnotation: MKPointAnnotation!
    // MARK: - Draw Journey
    var theJourney:     Journey?{
        didSet{
            
            
            if theJourney != oldValue {
                lastPolygonIndex = 0
                //remove previous polylines
                mapView.removeOverlays(mapView.overlays)
            }
            // Add current polylines
            addPolylines()
            // new journey set it should show in the middle of the map
            updateUserLocationAndCalculateRegion()
        }
    }
    // Draw the tracking lines
    func addPolylines() {
        
        // polylines
        if theJourney != nil && theJourney!.coords != nil {
            
            var coordsToDraw:[Coord]!
            if lastPolygonIndex < 1 {
                // First time; draw all coordinates in the journey
                coordsToDraw = theJourney!.coords!.array as! [Coord]
                removeBeginAndEndAnnotations()
                addBeginAndEndAnnotations()
            }else{
                // Then only new added coordinates should be added on the map
                coordsToDraw = [Coord]()
                for i in lastPolygonIndex..<theJourney!.coords!.count {
                    let coord = (theJourney!.coords?.object(at: i)) as! Coord
                    coordsToDraw.append(coord)
                    print("added index: \(i)")
                }
            }
            
            // remember last drawed polygon index
            lastPolygonIndex = theJourney!.coords!.count - 1
            
            var locations = coordsToDraw.map { $0.coord2D }
            let polyline = MKPolyline(coordinates: &locations, count: locations.count)
            
            mapView.add(polyline)
            removeEndAnnotation()
            addEndAnnotation()

            
        }
        
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
            
        }else{
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationView") ?? MKAnnotationView()
            annotationView.image = UIImage(named: "place icon")
            annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView.canShowCallout = true
            return annotationView
        }
    }
    func addBeginAndEndAnnotations(){
        addBeginAnnotation()
        addEndAnnotation()
    }
    func addBeginAnnotation(){
        guard let first = theJourney?.coords?.array.first else {return}
        let firstCoord = first as! Coord

        firstAnnotation = MKPointAnnotation()
        firstAnnotation.coordinate = CLLocationCoordinate2D(latitude: firstCoord.coord2D.latitude, longitude: firstCoord.coord2D.longitude)
        mapView.addAnnotation(firstAnnotation)
    }
    
    func addEndAnnotation(){
        guard let last = theJourney?.coords?.array.last else {return}
        let lastCoord = last as! Coord
   
        lastAnnotation = MKPointAnnotation()
        lastAnnotation.coordinate = CLLocationCoordinate2D(latitude: lastCoord.coord2D.latitude, longitude: lastCoord.coord2D.longitude)
        mapView.addAnnotation(lastAnnotation)
    }
    func removeBeginAndEndAnnotations(){
        removeBeginAnnotation()
        removeEndAnnotation()
    }
    func removeBeginAnnotation(){
        if let anno = firstAnnotation {
            mapView.removeAnnotation(anno)
        }
    }
    func removeEndAnnotation(){
        if let anno = lastAnnotation {
            mapView.removeAnnotation(anno)
        }
    }

    // Render the lines and/or circles
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    
            if overlay is MKCircle {
                let renderer = MKCircleRenderer(overlay: overlay)
                renderer.fillColor = UIColor.yellow.withAlphaComponent(0.5)
                renderer.strokeColor = UIColor.orange
                renderer.lineWidth = 2
                return renderer
                
            } else if overlay is MKPolyline {
                let renderer = MKPolylineRenderer(overlay: overlay)
                renderer.strokeColor = UIColor.green
                renderer.lineWidth = 3
                return renderer
            }
            
            return MKOverlayRenderer()
        
    }

    func setup(){
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
    }
    private var defaultRange:MKCoordinateRegion{
        get{
            let userLocation = mapView.userLocation
            return  MKCoordinateRegionMakeWithDistance(userLocation.location!.coordinate, 500, 500)
        }
    }
    private var doDefaultRegionZoomWithFirstUserLocationFound:Bool = false {
        didSet{
            if doDefaultRegionZoomWithFirstUserLocationFound == oldValue {return}
            
            let userLocation = mapView.userLocation
            
            if userLocation.location != nil {
                mapView.setRegion(defaultRange, animated: true)
            }
        }
    }
    // MARK: - Move user position for every position update
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        // set default region size with first run
        doDefaultRegionZoomWithFirstUserLocationFound = true
        updateUserLocationAndCalculateRegion()
    }
    func updateUserLocationAndCalculateRegion(){
        mapView.setRegion(mapRegion, animated: true)
        addPolylines()
    }
    // MARK: - Find the map region according to recorded coordinates in the DB
    private var mapRegion: MKCoordinateRegion {
        get{
            
            if theJourney == nil || theJourney!.coords == nil { return defaultRange }
            
            if let initialLocation = theJourney?.coords?.array.first {
                
                let initialCoord = initialLocation as! Coord
               
                var minLat = initialCoord.latitude
                var minLng = initialCoord.longitude
                var maxLat = minLat
                var maxLng = minLng
                
                let locations = theJourney?.coords?.array as! [Coord]
                
                for location in locations{
                    
                    minLat = min( minLat, location.latitude )
                    minLng = min( minLng, location.longitude )
                    maxLat = max( maxLat, location.latitude )
                    maxLng = max( maxLng, location.longitude )
                }
                
                return MKCoordinateRegion(
                    center: CLLocationCoordinate2D( latitude     : ( (minLat + maxLat)/2  ) , longitude     : ( (minLng + maxLng)/2  ) ),
                    span  : MKCoordinateSpan      ( latitudeDelta: ( (maxLat - minLat)*1.2) , longitudeDelta: ( (maxLng - minLng)*1.2) )
                )
            }
            return defaultRange
        }
    }
    

}

