//
//  TrackerModel.swift
//  MyJourney
//
//  Created by dejaWorks on 14/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreLocation


// MARK: - TrackerModel is heart of the application;
// To have an easy access, (also its nature needs to be) it was made as Singleton


private let _shared = TrackerModel()
class TrackerModel: NSObject,
    CLLocationManagerDelegate{

    // MARK: - SHARED INSTANCE
    class var shared : TrackerModel {
        return _shared
    }
    
    //  MARK: - private vars
    private var _isTracking:Bool = false
    private var _startLocation: CLLocation!
    private var _locationManager: CLLocationManager!
    private var _trackingJourney:     Journey?
    
    //  MARK: - public vars
    var isTracking:Bool {
        get { return _isTracking }
    }
    var trackingJourney:Journey? {
        get { return _trackingJourney }
    }
    
    // Run locarion manager and ask authorisation for tracking
    func setup(){
        _locationManager = CLLocationManager()
        
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.delegate = self
        
        _locationManager.requestWhenInUseAuthorization()
        _locationManager.requestAlwaysAuthorization()
        _locationManager.startUpdatingLocation()

    }
    func startTracking(){
        _trackingJourney = JourneyDS.shared.newEmptyJourney
        _trackingJourney?.name = "🏃 Current Tracking"
        _startLocation = nil
        _isTracking = true
    }
    func stopTracking(){
        _trackingJourney?.name = "Journey \(JourneyDS.shared.totalQty)"
        JourneyDS.shared.saveContext()
        
        // This last notification of tracking helps to change Journey name; from current to journey xx number.
        NotificationCenter.default.post(name: .didUpdateLocationsEvent, object: self)
        _isTracking = false
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // if tracking is not active; do nothing
        if !isTracking { return }
        
        let latestLocation: CLLocation = locations[locations.count - 1]
        let latitude = String(format: "%.4f", latestLocation.coordinate.latitude)
        let longitude = String(format: "%.4f", latestLocation.coordinate.longitude)
        //let horizontalAccuracy =  String(format: "%.4f",latestLocation.horizontalAccuracy)
        //let altitude = String(format: "%.4f", latestLocation.altitude)
        //let verticalAccuracy = String(format: "%.4f", latestLocation.verticalAccuracy)
        
        
        if _startLocation != nil {
            let distanceBetween: CLLocationDistance =
                latestLocation.distance(from: _startLocation)
            let distance = String(format: "%.2f", distanceBetween)
            
            if distanceBetween < 1 {
                print("Distance is short (\(distance)). No need to record.")
                NotificationCenter.default.post(name: .didUpdateLocationsEvent, object: self)
                return
            }
        }
        
        
        let coord = CoordDS.shared.newEmptyCoord
            coord.latitude = latestLocation.coordinate.latitude
            coord.longitude = latestLocation.coordinate.longitude
        
        _trackingJourney?.addToCoords(coord)

        _startLocation = latestLocation
        
        // During debugging below code is useful
        print("latitude: \(latitude) | longitude: \(longitude) | recorded coord qty: \(String(describing: _trackingJourney?.coords?.count))")
        
        // Custom notification/event for informing main view about update
        NotificationCenter.default.post(name: .didUpdateLocationsEvent, object: self)
    }
    // MARK: - Error handler
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locationManager didFailWithError")
    }
    
}
