//
//  JourneyDetailVC.swift
//  MyJourney
//
//  Created by dejaWorks on 14/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class JourneyDetailVC: UIViewController,
    JourneyDetailVCDelegate,
    MKMapViewDelegate {
    

    // MARK: - UI Outlet Connections
    @IBOutlet weak var menuButtonOutlet: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var startStopButtonOutlet: UIButton!
    @IBOutlet weak var journeyInfoHolder: UIView!
    
    
    
    
    // MARK: - UI Actions
    @IBAction func menuButton(_ sender: Any) { showJourneyList() }
    @IBAction func startStopButton(_ sender: Any) { startStopTracking() }
    
    // MARK: - Controllers
    fileprivate var mapVC: MapVC!
    fileprivate var journeyInfoVC: JourneyCellVC!
    
    // VARs
    var journeyListVC:JourneyListVC!
    let tracker = TrackerModel.shared
    var selectedJourney:Journey? {
        didSet{
            mapVC.theJourney         = selectedJourney
            journeyInfoVC.theJourney = selectedJourney
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tracker.setup()
        
        
        mapVC = MapVC(mapView)
        mapVC.setup()
        
        
        journeyInfoVC = JourneyCellVC(journeyInfoHolder)
        journeyInfoVC.setup()
        
        addObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Observer for tracking updates. It is listening to Singleton TrackerModel
    func addObservers(){
        NotificationCenter.default.addObserver(
            self,
            selector:   #selector(self.locationUpdated),
            name:       .didUpdateLocationsEvent,
            object:    tracker)
        
    }
    
    // DELEGATE func of journeyListVC
    // When a journey is selected; it is injecting into mapVC (for drawing the path) and journeyInfoVC (main view's journey info)
    func selectedJourney(_ journey: Journey) {
        selectedJourney = journey
    
    }
    
    func locationUpdated(_ notification:Notification){
        if tracker.isTracking && selectedJourney == tracker.trackingJourney{
            journeyInfoVC.theJourney = tracker.trackingJourney
        }
    }
    // MARK: - Creation / open/ close the Journey List View
    func showJourneyList(){
        // INIT if journeyListVC is not created yet instantiate
        if journeyListVC == nil {
            journeyListVC = self.storyboard!.instantiateViewController(withIdentifier: "JourneyListVC") as! JourneyListVC
            journeyListVC.delegate = self
            // "didMove toParentViewController" is recomended according to WWDC 2011 https://developer.apple.com/videos/play/wwdc2011/102/
            journeyListVC.didMove(toParentViewController: self)
            journeyListVC.view.frame.origin.x = self.view.frame.width
            
        }
        
        if !journeyListVC.isSideMenuPresent {
            
            self.view.addSubview(journeyListVC.view!)
            self.addChildViewController(journeyListVC)
            journeyListVC.view.layoutIfNeeded()
            journeyListVC.openSideMenu()
            
            // Prevent multiply clicks
            menuButtonOutlet.isEnabled = false
            
            //Hide menu button
            UIView.animate(withDuration: 0.2, animations: {
                self.menuButtonOutlet.alpha = 0
            })
        }else{
            journeyListVC.closeSideMenu()
        }
        
    }
    // Show, hidden menu button
    func willClose() {
        menuButtonOutlet.isEnabled = true
        UIView.animate(withDuration: 0.3, animations: {
            self.menuButtonOutlet.alpha = 1
        })
    }

    // MARK: - Tracking action
    func startStopTracking(){
        if tracker.isTracking {
            tracker.stopTracking()
            startStopButtonOutlet.setTitle("START", for: .normal)
        }else{
            tracker.startTracking()
            selectedJourney = tracker.trackingJourney
            
            startStopButtonOutlet.setTitle("STOP", for: .normal)
        }
    }
}
