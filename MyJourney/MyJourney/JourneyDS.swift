//
//  JourneyDS.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import UIKit
import CoreData
// MARK: - Journey Data Service
private let _shared = JourneyDS()
class JourneyDS: BaseDS {
    
    // MARK: - SHARED INSTANCE
    class var shared : JourneyDS {
        return _shared
    }
    // Default test coords
    // This data will be loaded automatically if there is no any data in the DB
                                // lat          long
    var testJourney:[Double] = [53.672086, -1.642070,
                                53.671902, -1.640858,
                                53.671851, -1.640579,
                                53.672226, -1.640515,
                                53.672131, -1.639335,
                                53.671883, -1.638326,
                                53.671400, -1.638423,
                                53.671171, -1.637243]
    // MARK: - All exsisting Journey count
    var totalQty:Int{
        get{
            let request:NSFetchRequest<Journey>    = Journey.fetchRequest()
            
            do {
                let fetchedJourney = try context.fetch(request as! NSFetchRequest<NSFetchRequestResult>) as! [Journey]
                return fetchedJourney.count
                
            } catch let error {
                print("Failed to fetch Journey: \(error)")
                return 0
            }
        }
    }
    // MARK: - Instantiate Journey Object
    var newEmptyJourney:Journey{
        get{
            let emptyJourney = NSEntityDescription.insertNewObject(forEntityName: "Journey", into: context) as! Journey
            setDefaults(emptyJourney)
            
            //add other properties
            
            return emptyJourney
        }
    }
    func createDefaultJourney(){
        
        let journey = newEmptyJourney
            journey.name = "My Test Journey"
        
        var index = 0
        repeat{
        let coord = CoordDS.shared.newEmptyCoord
            let time = NSDate()
            let interval = 5.0 * Double(index)
            coord.createdAt = time.addingTimeInterval(interval)
            
            coord.latitude  = testJourney[index]
            index += 1
            coord.longitude = testJourney[index]
            index += 1
            
            
        journey.addToCoords(coord)
            
        }while index < testJourney.count
        
        saveContext()
    }

}
