//
//  Base+CoreDataProperties.swift
//  MyJourney
//
//  Created by dejaWorks on 13/04/2017.
//  Copyright © 2017 dejaWorks. All rights reserved.
//

import Foundation
import CoreData


extension Base {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Base> {
        return NSFetchRequest<Base>(entityName: "Base")
    }

    @NSManaged public var name: String?
    @NSManaged public var updatedAt: NSDate?
    @NSManaged public var createdAt: NSDate?

}
